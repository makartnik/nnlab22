﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NNLab22
{
    public class Neuron
    {
        public double[] W { get; }

        public const double Alpha = 1.0;

        public Neuron(int size)
        {
            W = Enumerable.Repeat(0.0, size).ToArray();
            Random random = new Random();
            for (int i = 0; i < size; ++i)
            {
                W[i] = random.NextDouble();
            }
        }

        public double F(List<double> inputData)
        {
            if (inputData.Count != W.Length)
                throw new ArgumentException(nameof(inputData));
            double sum = inputData.Select((v, i) => v * W[i]).Sum();
            return 1.0 / (1 + Math.Pow(Math.E, -Alpha * sum));
        }
    }
}