﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NNLab22
{
    public class NeuralNetwork : FunctionBase
    {
        private IFunction _function;

        private List<Neuron> _hiddensNeurons;

        private Neuron _outputNeuron;

        private double _minY, _maxY;

        private double _nu = 0.5;


        public NeuralNetwork(IFunction function, int hiddenCount, double minY, double maxY)
        {
            _function = function;
            _hiddensNeurons = new List<Neuron>();
            for (int i = 0; i < hiddenCount; ++i)
            {
                _hiddensNeurons.Add(new Neuron(1));
            }
            _outputNeuron = new Neuron(hiddenCount);

            _minY = minY;
            _maxY = maxY;

            _nu = 1;
        }

        private double ScaleAnswer(double y)
        {
            return y * (_maxY - _minY) + _minY;
        }

        private double UnscaleAnswer(double y)
        {
            return (y - _minY) / (_maxY - _minY);
        }

        public void Train(int iterationCount, double minError, double minX, double maxX, int pointCount)
        {
            for (int i = 0; i < iterationCount; i++)
            {
                double error = 0;
                for (int j = 0; j < pointCount; j++)
                {
                    double x = minX + j * (maxX - minX) / (pointCount - 1);
                    double y = GetUnscaledValue(x);
                    double t = UnscaleAnswer(_function.GetValue(x)); //target
                    double currentError = Math.Pow(t - y, 2) / 2;
                    error += currentError;
                    Update(x, y, t);
                }
                if (error < minError)
                    return;
            }
        }

        private void Update(double x, double y, double t)
        {
            var hiddenInput = new[] {x}.ToList();
            double delta = y * (1 - y) * (t - y);

            for (int i = 0; i < _outputNeuron.W.Length; i++)
                _outputNeuron.W[i] += _nu * delta * _hiddensNeurons[i].F(hiddenInput);

            for (int i = 0; i < _hiddensNeurons.Count; i++)
                _hiddensNeurons[i].W[0] += _nu * delta * _outputNeuron.W[i] * _hiddensNeurons[i].F(hiddenInput)
                                           * (1 - _hiddensNeurons[i].F(hiddenInput))
                                           * x;
        }

        //0-1
        private double GetUnscaledValue(double x)
        {
            var hiddenInput = new[] {x}.ToList();
            var inputData = _hiddensNeurons.Select(n => n.F(hiddenInput)).ToList();
            double y = _outputNeuron.F(inputData);
            return y;
        }

        //miny-maxy
        public override double GetValue(double x)
        {
            return ScaleAnswer(GetUnscaledValue(x));
        }
    }
}