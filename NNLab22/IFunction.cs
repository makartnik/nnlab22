﻿using System.Drawing;
using ZedGraph;

namespace NNLab22
{
    public interface IFunction
    {
        double GetValue(double x);

        LineItem GetGraph(double minX, double maxX, int count, Color color, string name);
    }
}