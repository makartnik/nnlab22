﻿using System.Drawing;
using ZedGraph;

namespace NNLab22
{
    public abstract class FunctionBase : IFunction
    {
        public abstract double GetValue(double x);

        public LineItem GetGraph(double minX, double maxX, int count, Color color, string name = "")
        {
            var points = new PointPairList();
            for (int i = 0; i < count; ++i)
            {
                double x = minX + i * (maxX - minX) / (count - 1);
                double y = GetValue(x);
                points.Add(x,y);
            }

            var curve = new LineItem(name, points, color, SymbolType.None);
            return curve;
        }
    }
}