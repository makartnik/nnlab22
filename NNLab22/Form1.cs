﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NNLab22
{
    public partial class Form1 : Form
    {
        Function _function = new Function();
        private NeuralNetwork _network;

        public Form1()
        {
            InitializeComponent();
            zedGraphControl1.GraphPane.Title.IsVisible =
                zedGraphControl1.GraphPane.XAxis.Title.IsVisible =
                    zedGraphControl1.GraphPane.Y2Axis.Title.IsVisible = false;
        }

        private void zedGraphControl1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            zedGraphControl1.GraphPane.CurveList.Clear();
            zedGraphControl1.GraphPane.CurveList.Add(_function.GetGraph(-1, 1, 100, Color.Coral, "3 * x * exp(cos(x))"));
            _network = new NeuralNetwork(_function, 10, -7, 6);
            _network.Train(iterationCount: 100,
                minError: 0.0010,
                minX: -1,
                maxX: 1,
                pointCount: 100);
            zedGraphControl1.GraphPane.CurveList.Add(_network.GetGraph(-1, 1, 100, Color.Blue, "Neural"));
            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
        }
    }
}