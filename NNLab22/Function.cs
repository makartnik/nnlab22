﻿using System;

namespace NNLab22
{
    public class Function : FunctionBase
    {
        public override double GetValue(double x)
        {
            return 3 * x * Math.Exp(Math.Cos(x));
        }
    }
}